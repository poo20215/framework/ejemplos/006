<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Peliculas;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
        $salida=Peliculas::find()->select(['portada'])->where(["pelicula_de_la_semana" => 1])->one();
        return $this->render("index",['datos'=> $salida]);
    }
    
    public function actionVer($portada)
    {
        $modelo= Peliculas::find()->where(["portada" => $portada])->one();
        return $this->render("ver",["modelo"=> $modelo]);
    }
    
    public function actionListado()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Peliculas::find()]);
        return $this->render("listado",["dataProvider" => $dataProvider]);
    }
    
    public function actionCategoria($categoria)
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Peliculas::find()->where(['categoria'=>$categoria])]);
        return $this->render("categoria_gv",["dataProvider" => $dataProvider]);
    }
    
    public function actionCategorias()
    {
        $resultado=Yii::$app->db->createCommand("select distinct categoria from peliculas")->queryAll();
        return $this->render("categorias",["resultado" => $resultado]);
    }
    
    public function actionCategoria2($categoria)
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Peliculas::find()->where(['categoria'=>$categoria])]);
        return $this->render("categoria_lv",["dataProvider" => $dataProvider]);
    }
    
    public function actionDestacadas()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Peliculas::find()->where(['destacada'=>1])]);
        return $this->render("categoria_lv",["dataProvider" => $dataProvider]);
    }
    
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $descripcion
 * @property int|null $duracion
 * @property string|null $fecha_estreno
 * @property string $categoria
 * @property string $portada
 * @property string|null $director
 * @property int|null $destacada
 * @property int|null $pelicula_de_la_semana
 *
 * @property Fotos[] $fotos
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'categoria', 'portada'], 'required'],
            [['duracion', 'destacada', 'pelicula_de_la_semana'], 'integer'],
            [['fecha_estreno'], 'safe'],
            [['titulo', 'director'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 100],
            [['categoria'], 'string', 'max' => 20],
            [['portada'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'duracion' => 'Duracion',
            'fecha_estreno' => 'Fecha Estreno',
            'categoria' => 'Categoria',
            'portada' => 'Portada',
            'director' => 'Director',
            'destacada' => 'Destacada',
            'pelicula_de_la_semana' => 'Pelicula De La Semana',
        ];
    }

    /**
     * Gets query for [[Fotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::className(), ['pelicula_a_la_que_pertenece' => 'id']);
    }
}

<?php
use yii\helpers\Html;
?>

<table>
    <tr><td rowspan="4"><?= Html::img('@web/img/' . $model->portada,['class'=>'img-fluid', 'style'=>'width:300px'])?></td><td><?= "Titulo: " . $model->titulo ?></td></tr>
    <tr><td><?= "Duración: " . $model->duracion ?></td></tr>
    <tr><td><?= "Director: " . $model->director ?></td></tr>
    <tr><td><?= "Categoria: " . $model->categoria ?></td></tr>
    <tr><td><?= Html::a("Leer mas...", ['site/ver', 'portada' => $model->portada], ['class' => 'profile-link']) . "<br>";  ?></td></tr>
    
</table>

<?php

use yii\grid\GridView;
use yii\bootstrap4\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'titulo',
        [
            'label'=>'Categoria',
            'format'=>'raw',
            'value' => function($data){
                $url = ['site/categoria','categoria'=>$data->categoria];
                return Html::a($data->categoria, $url, ['class' => 'btn btn-primary']); 
            }
        ],
        [
            'label'=>'Cartel',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/img/' . $data->portada;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'width:300px']);
            }
        ],
        'fecha_estreno',
        
    ]]);

?>
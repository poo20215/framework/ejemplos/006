<?php

use yii\grid\GridView;
use yii\bootstrap4\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'titulo',
        [
            'label'=>'Cartel',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/img/' . $data->portada;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'width:300px']);
            }
        ],
        'fecha_estreno',
        
    ]]);

?>


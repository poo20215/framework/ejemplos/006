<?php

use yii\widgets\DetailView;
use yii\bootstrap4\Html;

echo DetailView::widget([
    'model' => $modelo,
    'attributes' => [
        'id',
        'titulo',
        'duracion',
        'fecha_estreno',
        'categoria',
        [
            'label'=>'Portada',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/img/' . $data->portada;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'height:100px']);
            }
        ],
        'director',
        'destacada',
            ]
]);
